#ifndef CARD_H
#define CARD_H
#include <SFML/Graphics.hpp>
#include "Card.hpp"

enum class Result : short{LOWER = 1, EQUAL = 2, HIGHER = 3};
enum class Values : short{TWO = 1, THREE = 2, FOUR = 3, FIVE = 4, SIX = 5, SEVEN = 6, EIGHT = 7, NINE = 8, TEN = 9, JACK = 10, QUEEN = 11, KING = 12, ACE = 13};
enum class Suits : short{CLUBS = 1, HEARTS = 2, DIAMONDS = 3, SPADES = 4};

class Card
{
    public:
        Card(int value, int suit);

        virtual ~Card();

        ///////////
        //GETTERS//
        ///////////
        unsigned short getValue();
        unsigned short getSuit();
        std::string getName();
        ///////////
        //SETTERS//
        ///////////
        void setValue(unsigned short);
        void setSuit(unsigned short);

        short compare(Card);
        static void swapCards(Card&, Card&);

    protected:
    private:
        sf::Texture m_texture;
        sf::Sprite m_sprite;

        std::string m_name;

        unsigned short m_value;
        unsigned short m_suit;
};

#endif // CARD_H
