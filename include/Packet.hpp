#ifndef PACKET_H
#define PACKET_H
#include "Card.hpp"
#include <iostream>

class Packet
{
    public:
        Packet();
        virtual ~Packet();
    protected:
    private:
        std::vector<Card> m_cards;
};

#endif // PACKET_H
