#ifndef GAME_H
#define GAME_H
#include <iostream>
#include "Packet.hpp"
#include "Player.hpp"

class Game
{
    public:
        Game();
        virtual ~Game();
        void dealCards();
    protected:
    private:
        Packet m_packet;
        std::vector<Player> m_players;
};

#endif // GAME_H
