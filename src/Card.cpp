#include "Card.hpp"

Card::Card(int value, int suit)
{
    m_value = value;
    m_suit = suit;

    switch(m_value)
    {
        case (static_cast<short>(Values::TWO)): {
            m_name = "Deux";
            break;
        }
        case (static_cast<short>(Values::THREE)): {
            m_name = "Trois";
            break;
        }
        case (static_cast<short>(Values::FOUR)): {
            m_name = "Quatre";
            break;
        }
        case (static_cast<short>(Values::FIVE)): {
            m_name = "Cinq";
            break;
        }
        case (static_cast<short>(Values::SIX)): {
            m_name = "Six";
            break;
        }
        case (static_cast<short>(Values::SEVEN)): {
            m_name = "Sept";
            break;
        }
        case (static_cast<short>(Values::EIGHT)): {
            m_name = "Huit";
            break;
        }
        case (static_cast<short>(Values::NINE)): {
            m_name = "Neuf";
            break;
        }
        case (static_cast<short>(Values::TEN)): {
            m_name = "Dix";
            break;
        }
        case (static_cast<short>(Values::JACK)): {
            m_name = "Valet";
            break;
        }
        case (static_cast<short>(Values::QUEEN)): {
            m_name = "Dame";
            break;
        }
        case (static_cast<short>(Values::KING)): {
            m_name = "Roi";
            break;
        }
        case (static_cast<short>(Values::ACE)): {
            m_name = "As";
            break;
        }
    }

    m_name += " de ";

    switch(m_suit)
    {
        case (static_cast<short>(Suits::CLUBS)): {
            m_name += "trefle";
            break;
        }
        case (static_cast<short>(Suits::HEARTS)): {
            m_name += "coeur";
            break;
        }
        case (static_cast<short>(Suits::DIAMONDS)): {
            m_name += "carreau";
            break;
        }
        case (static_cast<short>(Suits::SPADES)): {
            m_name += "pique";
            break;
        }

    }
}

short Card::compare(Card card)
{
    if(m_value > card.getValue())
    {
        return static_cast<short>(Result::HIGHER);
    }
    else if(m_value < card.getValue())
    {
        return static_cast<short>(Result::LOWER);
    }
    else if(m_value == card.getValue())
    {
        return static_cast<short>(Result::EQUAL);
    }

    return 0;
}

void Card::swapCards(Card& card1, Card& card2)
{
    Card temporaryCard = card1;
    card1 = card2;
    card2 = temporaryCard;
}

unsigned short Card::getValue()
{
    return m_value;
}
unsigned short Card::getSuit()
{
    return m_suit;
}
std::string Card::getName()
{
    return m_name;
}

void Card::setSuit(unsigned short suit)
{
    m_suit = suit;
}
void Card::setValue(unsigned short value)
{
    m_value = value;
}

Card::~Card()
{
    //dtor
}
