#include <SFML/Graphics.hpp>
#include <iostream>
#include "Card.hpp"

int main()
{
    std::vector<Card> cards;

    for(int i=1; i<=4; i++) {
        for(int j=1; j<=13; j++){
            Card c(j, i);
            cards.push_back(c);
            std::cout << c.getName() << "\n";
        }
        std::cout << std::endl;
    }


    return 0;
}
